require "ostruct"

module Omnioauthecho
  extend self

  class Error < StandardError; end
  module Errors
    class InvalidHeader < Error; end
    class AuthenticationFailed < Error; end
    class UnsupportedServiceProvider < Error; end
    class FailedServiceProviderRequest < Error; end
  end

  PROVIDERS = { twitter: 'https://api.twitter.com/1/account/verify_credentials.json' }
  TWITTER = "http://twitter.com/"

  module ControllerMethods
#####    extend ActiveSupport::Concern
    def authenticate_with_oauth_echo
      service_provider = request.headers['X-Auth-Service-Provider']
      credentials = request.headers['X-Verify-Credentials-Authorization']

      raise Errors::InvalidHeader if validate_request_header(service_provider, credentials)
      case service_provider
      when TWITTER
        return authenticate_with_twitter(credentials)
      else
        raise Errors::UnsupportedServiceProvider, service_provider
      end
    end

    private

    def validate_request_header(service_provider, credentials)
      false if service_provider.blank? || auth.blank?
    end

    def authenticate_with_twitter(credentials)
      raise Errors::AuthenticationFailed unless validate_consumer_key(credentials)
      begin
        res = verify_credentials(credentials)
      rescue => e
        raise Errors::AuthenticationFailed, e.message, e.backtrace
      end
      case res.code
      when '200'
        auth_hash = create_auth_hash(JSON.parse(res.body))
        logger.info("twitter response code: #{res.code}, twitter id: #{auth_hash.uid}")
        return auth_hash
      when '401'
        raise Errors::AuthenticationFailed, res.body
      else
        raise Errors::ServiceProviderRequestFailed, res.body
      end
    end

    def verify_credentials(credentials)
      proxy_addr, proxy_port = get_proxy
      http = Net::HTTP::Proxy(proxy_addr, proxy_port).new("api.twitter.com", 443)
      http.use_ssl = true
      http.get("/1.1/account/verify_credentials.json", {
        "Authorization" => auth,
        "Content-Type" => "application/json"})
    end

    def create_auth_hash(json)
      AuthHash.new("twitter", json["id_str"], OpenStruct.new({ name: json["name"] }), NilCredentials, OpenStruct.new({
        raw_info: OpenStruct.new({ id: json["id_str"], name: json["name"] }) }) )
    end

    def get_proxy
      uri = URI.parse(ENV['http_proxy'] || ENV['HTTP_PROXY'])
      return [uri.host, uri.port]
    rescue => e
      raise Errors::UriParseFailed, e.message, e.backtrace
    end


    def validate_consumer_key(credentials)
      return false if (credentials =~ /[, \n]oauth_consumer_key=\"(.*?)\"/).nil? || Settings.twitter.api.api_keys.grep(/^#{$1}$/).empty?
      return true
    end
  end

end
