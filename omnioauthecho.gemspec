$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "omnioauthecho/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "omnioauthecho"
  s.version     = Omnioauthecho::VERSION
  s.authors     = ["saber_chica"]
  s.email       = ["saber.chica@gmail.com"]
  s.homepage    = "https://github.com/sohda/omnioauthecho"
  s.summary     = "provide a OAuthEcho client"
  s.description = "provide a OAuthEcho client"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.1.6"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "ammeter"
end
